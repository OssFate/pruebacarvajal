﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PruebaCarvajal.Api.Data.Entities
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        [ForeignKey("IdentificationType")]
        public int IdentificationTypeId { get; set; }
        public string Identification { get; set; }
        [StringLength(8)]
        public string Password { get; set; }
        [EmailAddress]
        public string Email { get; set; }
        public IdType IdentificationType { get; set; }
    }
}