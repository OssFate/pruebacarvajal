﻿using Microsoft.EntityFrameworkCore;
using PruebaCarvajal.Api.Data.Entities;

namespace PruebaCarvajal.Api.Data
{
    public class PersonContext : DbContext
    {
        public PersonContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Person> Person { get; set; }
        public DbSet<IdType> IdType { get; set; }
    }
}