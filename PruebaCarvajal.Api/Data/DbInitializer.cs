﻿using System.Linq;
using PruebaCarvajal.Api.Data.Entities;

namespace PruebaCarvajal.Api.Data
{
    public class DbInitializer
    {
        public static void Initialize(PersonContext context)
        {
            if (context.IdType.Any()) return;

            var ids = new IdType[]
            {
                new IdType { Name = "Cedula", Value = "CC" },
                new IdType { Name = "Registro Civil", Value = "RC" },
                new IdType { Name = "Tarjeta de Identidad", Value = "TI" },
                new IdType { Name = "Cedula Extranjera", Value = "CE" },
            };
            
            context.IdType.AddRange(ids);
            context.SaveChanges();
        }
    }
}