﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PruebaCarvajal.Api.Data;
using PruebaCarvajal.Api.Data.Entities;

namespace PruebaCarvajal.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonController : ControllerBase
    {
        private readonly ILogger<PersonController> _logger;
        private readonly PersonContext _db;

        public PersonController(ILogger<PersonController> logger, PersonContext db)
        {
            _logger = logger;
            _db = db;
        }

        [HttpGet]
        [Route("GetIdTypes")]
        public async Task<ActionResult<IEnumerable<IdType>>> GetIdTypes()
        {
            try
            {
                if (!await _db.IdType.AnyAsync()) return NotFound("No se encontraron elementos");

                return Ok(await _db.IdType.ToListAsync());
            }
            catch (Exception e)
            {
                return Problem(statusCode: 500, title: "Error", detail: $"{e.Message} - {e.InnerException?.Message}");
            }
        }
        
        [HttpGet]
        [Route("GetPerson/{id:int}")]
        public async Task<ActionResult<IEnumerable<Person>>> GetPerson(int id)
        {
            try
            {
                var person = await _db.Person.FirstOrDefaultAsync(p => p.Id == id);

                if (person == null) return NotFound("Id de persona no se encuentra en la base de datos");

                return Ok(person);
            }
            catch (Exception e)
            {
                return Problem(statusCode: 500, title: "Error", detail: $"{e.Message} - {e.InnerException?.Message}");
            }
        }
        
        [HttpGet]
        [Route("GetPeople")]
        public async Task<ActionResult<IEnumerable<Person>>> GetPeople()
        {
            try
            {
                if (!await _db.Person.AnyAsync()) return NotFound("No se encontraron elementos");

                return Ok(await _db.Person.Include(i => i.IdentificationType).ToListAsync());
            }
            catch (Exception e)
            {
                return Problem(statusCode: 500, title: "Error", detail: $"{e.Message} - {e.InnerException?.Message}");
            }
        }

        [HttpPost]
        [Route("UpdatePerson")]
        public async Task<ActionResult<bool>> UpdatePerson(Person updatePerson)
        {
            try
            {
                var person = await _db.Person.FirstOrDefaultAsync(p => p.Id == updatePerson.Id);

                if (person == null) return NotFound("Persona no se encuentra en la base de datos");

                _db.Entry(person).State = EntityState.Detached;
                
                _db.Person.Attach(updatePerson).State = EntityState.Modified;

                return Ok(await _db.SaveChangesAsync() > 0);
            }
            catch (Exception e)
            {
                return Problem(statusCode: 500, title: "Error", detail: $"{e.Message} - {e.InnerException?.Message}");
            }
        }

        [HttpPost]
        [Route("AddPerson")]
        public async Task<ActionResult<int>> AddPerson(Person person)
        {
            try
            {
                if (person.IdentificationType != null)
                {
                    var idType = await _db.IdType.FirstOrDefaultAsync(i => i.Id == person.IdentificationType.Id ||
                                                                           i.Name == person.IdentificationType.Name ||
                                                                           i.Value == person.IdentificationType.Value);

                    if (idType == null) return NotFound("Tipo de identificacion no existe en la base de datos");
                    
                    person.IdentificationType = idType;
                }

                if (person.IdentificationTypeId > 0 && !await _db.IdType.AnyAsync(t => t.Id == person.IdentificationTypeId))
                    return NotFound("Tipo de identificacion no existe en la base de datos");

                var result = await _db.Person.AddAsync(person);
                await _db.SaveChangesAsync();

                return Ok(result.Entity.Id);
            }
            catch (Exception e)
            {
                return Problem(statusCode: 500, title: "Error", detail: $"{e.Message} - {e.InnerException?.Message}");
            }
        }
        
        [HttpDelete]
        [Route("DeletePerson/{id:int}")]
        public async Task<ActionResult<Person>> DeletePerson(int id)
        {
            try
            {
                var person = await _db.Person.FirstOrDefaultAsync(p => p.Id == id);

                if (person == null) return NotFound("Id de persona no se encuentra en la base de datos");
                
                var result = _db.Person.Remove(person);
                await _db.SaveChangesAsync();

                return Ok(result.Entity.Id);
            }
            catch (Exception e)
            {
                return Problem(statusCode: 500, title: "Error", detail: $"{e.Message} - {e.InnerException?.Message}");
            }
        }
    }
}