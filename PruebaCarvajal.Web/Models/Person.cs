﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace PruebaCarvajal.Web.Models
{
    public class Person
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("lastName")]
        public string LastName { get; set; }
        [JsonPropertyName("identificationTypeId")]
        public int IdentificationTypeId { get; set; }
        [JsonPropertyName("identification")]
        public string Identification { get; set; }
        [StringLength(8)]
        [JsonPropertyName("password")]
        public string Password { get; set; }
        [EmailAddress]
        [JsonPropertyName("email")]
        public string Email { get; set; }
        [JsonPropertyName("identificationType")]
        public IdType IdentificationType { get; set; }
    }
}