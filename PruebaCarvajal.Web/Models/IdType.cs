﻿using System.Text.Json.Serialization;

namespace PruebaCarvajal.Web.Models
{
    public class IdType
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("value")]
        public string Value { get; set; }
    }
}