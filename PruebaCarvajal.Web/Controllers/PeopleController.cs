﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PruebaCarvajal.Web.Models;

namespace PruebaCarvajal.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public PeopleController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        
        [HttpGet]
        [Route("GetPeople")]
        public async Task<IEnumerable<Person>> GetPeople()
        {
            using var client = new HttpClient();
            var result =
                await client.GetAsync(_configuration.GetValue<string>("AppSettings:PruebaUrl") + "Person/GetPeople");

            return result.IsSuccessStatusCode ? JsonSerializer.Deserialize<IEnumerable<Person>>(await result.Content.ReadAsStringAsync()) : null;
        }
        
        [HttpGet]
        [Route("GetIdTypes")]
        public async Task<IEnumerable<IdType>> GetIdTypes()
        {
            using var client = new HttpClient();
            var result =
                await client.GetAsync(_configuration.GetValue<string>("AppSettings:PruebaUrl") + "Person/GetIdTypes");

            return result.IsSuccessStatusCode ? JsonSerializer.Deserialize<IEnumerable<IdType>>(await result.Content.ReadAsStringAsync()) : null;
        }
        
        [HttpGet]
        [Route("GetPerson/{id:int}")]
        public async Task<Person> GetPerson(int id)
        {
            using var client = new HttpClient();
            var result =
                await client.GetAsync(_configuration.GetValue<string>("AppSettings:PruebaUrl") + $"Person/GetPerson/{id}");

            return result.IsSuccessStatusCode ? JsonSerializer.Deserialize<Person>(await result.Content.ReadAsStringAsync()) : null;
        }
        
        [HttpPost]
        [Route("AddPerson")]
        public async Task<bool> AddPerson(Person person)
        {
            using var client = new HttpClient();
            var result =
                await client.PostAsync(_configuration.GetValue<string>("AppSettings:PruebaUrl") + "Person/AddPerson",
                    new StringContent(JsonSerializer.Serialize(person), Encoding.UTF8, "application/json"));

            return result.IsSuccessStatusCode && Convert.ToInt32(await result.Content.ReadAsStringAsync()) > 0;
        }
        
        [HttpPost]
        [Route("UpdatePerson")]
        public async Task<bool> UpdatePerson(Person person)
        {
            using var client = new HttpClient();
            var result =
                await client.PostAsync(_configuration.GetValue<string>("AppSettings:PruebaUrl") + "Person/UpdatePerson",
                    new StringContent(JsonSerializer.Serialize(person), Encoding.UTF8, "application/json"));

            return result.IsSuccessStatusCode && Convert.ToBoolean(await result.Content.ReadAsStringAsync());
        }
        
        [HttpDelete]
        [Route("DeletePerson/{id:int}")]
        public async Task<bool> DeletePerson(int id)
        {
            using var client = new HttpClient();
            var result =
                await client.DeleteAsync(_configuration.GetValue<string>("AppSettings:PruebaUrl") +
                                         $"Person/DeletePerson/{id}");

            var content = await result.Content.ReadAsStringAsync();
            return result.IsSuccessStatusCode && Convert.ToInt32(content) > 0;
        }
    }
}