﻿import {Component, Inject} from "@angular/core";
import { Person } from "../global";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
})
export class PersonComponent {
  public people: Person[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Person[]>(baseUrl + 'people/getpeople').subscribe(result => {
      this.people = result;
    }, error => console.error(error));
  }
}
