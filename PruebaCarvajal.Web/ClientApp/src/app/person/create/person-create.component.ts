﻿import {Component, Inject} from "@angular/core";
import {IdType, Person} from "../../global";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-person-create',
  templateUrl: './person-create.component.html',
})
export class PersonCreateComponent {
  public person: Person;
  public idTypes: IdType[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router) {
    this.person = new Person();

    http.get<IdType[]>(baseUrl + 'people/getidtypes').subscribe(result => {
      this.idTypes = result;
    }, error => console.error(error));
  }

  createPerson() {
    this.http.post<any>(this.baseUrl + 'people/addperson', this.person).subscribe(result => {
      console.log(result);
      this.router.navigate(['/']);
    }, error => console.error(error));
  }
}
