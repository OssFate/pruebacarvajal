﻿import {Component, Inject} from "@angular/core";
import {IdType, Person} from "../../global";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-person-update',
  templateUrl: './person-update.component.html',
})
export class PersonUpdateComponent {
  public person: Person;
  public idTypes: IdType[];

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router, private route: ActivatedRoute) {
    http.get<IdType[]>(baseUrl + 'people/getidtypes').subscribe(result => {
      this.idTypes = result;
    }, error => console.error(error));

    let id = this.route.snapshot.children[0].params['id'];

    http.get<Person>(baseUrl + 'people/getperson/' + id).subscribe(result => {
      this.person = result;
    }, error => console.error(error));
  }

  updatePerson() {
    this.http.post<any>(this.baseUrl + 'people/updateperson', this.person).subscribe(result => {
      console.log(result);
      this.router.navigate(['/']);
    }, error => console.error(error));
  }
}
