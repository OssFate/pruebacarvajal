﻿import {Component, Inject} from "@angular/core";
import {Person} from "../../global";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
})
export class PersonDetailsComponent {
  public person: Person;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router, private route: ActivatedRoute) {
    let id = this.route.snapshot.children[0].params['id'];

    http.get<Person>(baseUrl + 'people/getperson/' + id).subscribe(result => {
      this.person = result;
    }, error => console.error(error));
  }

  goBack() {
    this.router.navigate(['/']);
  }
}
