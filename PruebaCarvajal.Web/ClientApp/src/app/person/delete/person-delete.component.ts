﻿import {Component, Inject} from "@angular/core";
import {IdType, Person} from "../../global";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-person-delete',
  templateUrl: './person-delete.component.html',
})
export class PersonDeleteComponent {
  public person: Person;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private router: Router, private route: ActivatedRoute) {
    this.person = new Person();
    let id = this.route.snapshot.children[0].params['id'];

    http.get<Person>(baseUrl + 'people/getperson/' + id).subscribe(result => {
      this.person = result;
    }, error => console.error(error));
  }

  deletePerson() {
    this.http.delete<any>(this.baseUrl + 'people/deleteperson/' + this.person.id).subscribe(result => {
      console.log(result);
      this.router.navigate(['/']);
    }, error => console.error(error));
  }
}
