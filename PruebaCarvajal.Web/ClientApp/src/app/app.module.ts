import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';

import { PersonComponent } from "./person/person.component";
import { PersonCreateComponent} from "./person/create/person-create.component";
import { PersonDetailsComponent } from "./person/details/person-details.component";
import { PersonUpdateComponent } from "./person/update/person-update.component";
import { PersonDeleteComponent } from "./person/delete/person-delete.component";

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    PersonComponent,
    PersonCreateComponent,
    PersonDetailsComponent,
    PersonUpdateComponent,
    PersonDeleteComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: PersonComponent, pathMatch: 'full' },
      { path: 'person/create', component: PersonCreateComponent },
      { path: 'person/details', component: PersonDetailsComponent, children: [
          { path: ':id', component: PersonDetailsComponent }
        ]},
      { path: 'person/update', component: PersonUpdateComponent, children: [
          { path: ':id', component: PersonUpdateComponent }
        ]},
      { path: 'person/delete', component: PersonDeleteComponent, children: [
          { path: ':id', component: PersonDeleteComponent }
        ]},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
