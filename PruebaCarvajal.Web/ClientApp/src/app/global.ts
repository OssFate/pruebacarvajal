export class Person {
  id: number;
  name: string;
  lastName: string;
  identificationTypeId: number;
  identification: string;
  email: string;
  password: string;
  IdentificationType: IdType;

  constructor() {
  }
}


export class IdType {
  id: number;
  name: string;
  value: string;
}
